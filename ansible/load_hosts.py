from os import listdir, getenv
from os.path import isfile, join


workers_files_path = getenv("WORKERS_DIR")
workers = ""
masters_files_path = getenv("MASTERS_DIR")
masters = ""
bastion_files_path = getenv("BASTION_DIR")
bastion = ""
bastion_ip = ""
os_key_path = getenv("OS_KEY_PATH")

workers_local_ip_files = [ f for f in listdir(workers_files_path) if isfile(join(workers_files_path, f)) and "local-ip" in f]
print(workers_local_ip_files)

if len(workers_local_ip_files):
    for worker_file in workers_local_ip_files:
        with open(join(workers_files_path, worker_file)) as file:
            local_ip = file.readline()
            worker_name = worker_file.replace("-local-ip","")
            new_worker = f"{worker_name} ansible_host={local_ip}\n"
            workers = workers + new_worker
    print(workers)
else:
    print("No worker files found")

masters_local_ip_files = [ f for f in listdir(masters_files_path) if isfile(join(masters_files_path, f)) and "local-ip" in f]
print(masters_local_ip_files)

if len(masters_local_ip_files):
    for master_file in masters_local_ip_files:
        with open(join(masters_files_path, master_file)) as file:
            local_ip = file.readline()
            master_name = master_file.replace("-local-ip","")
            new_master = f"{master_name} ansible_host={local_ip}\n"
            masters = masters + new_master
    print(masters)
else:
    print("No master files found")


bastion_public_ip_file = [ f for f in listdir(bastion_files_path) if isfile(join(bastion_files_path, f)) and "public-ip" in f]
print(bastion_public_ip_file)

if len(bastion_public_ip_file):
    for bastion_file in bastion_public_ip_file:
        with open(join(bastion_files_path, bastion_file)) as file:
            bastion_ip = file.readline()
            bastion_name = bastion_file.replace("-public-ip","")
            bastion = f"{bastion_name} ansible_host={bastion_ip}\n"
    print(bastion)

else:
    print("No master files found")

print("Bastion IP:", bastion_ip)

print("OS key path:", os_key_path)

print("Generating host file...")

hosts = ""

with open("hosts.template") as file:
    template = file.read()
    hosts = template.format(workers=workers, masters=masters, bastion=bastion, os_key_path=os_key_path, bastion_ip=bastion_ip)

with open("hosts", "w+") as file:
    file.write(hosts)

print("Host file genrated!")