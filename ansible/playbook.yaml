---
    # todo: CHECK FOR APT-TRAMPORT
    - name: Check Server
      hosts: all
      tasks:

        - name: Ensure servers are reachable
          ping:
    
    - name: Config for Network plugin
      hosts: [masters, workers]
      become: yes
      tasks:

        - name: Config bridge net filter
          blockinfile:
            create: yes
            path: /etc/sysctl.d/k8s.conf
            state: present
            block: |
                net.bridge.bridge-nf-call-ip6tables = 1
                net.bridge.bridge-nf-call-iptables = 1   

        - name: Disable SWAP since kubernetes can't work with swap enabled (1/2)
          shell: swapoff -a
        
        - name: Disable SWAP in fstab since kubernetes can't work with swap enabled
          replace:
            path: /etc/fstab
            regexp: '^([^#].*?\sswap\s+sw\s+.*)$'
            replace: '# \1'           
        
    - name: Install Kubectl
      hosts: [ masters, workers ]
      vars_files:
        - ./vars/proxies.yaml
      tasks:

        - name: Add Kubectl apt key.
          environment: "{{proxy_env}}"
          become: yes
          apt_key:
            url: https://packages.cloud.google.com/apt/doc/apt-key.gpg
            state: present

        - name: Add Kubectl repository.
          become: yes
          apt_repository:
            repo: deb https://apt.kubernetes.io/ kubernetes-xenial main
            state: present
            update_cache: yes

        - name: Install kubectl, kubeadm, kubectl
          become: yes
          apt:
            update_cache: yes
            pkg:
                - kubeadm
                - kubelet
                - kubectl

    - name: Before installing containerd
      hosts: [ masters, workers]
      become: yes
      tasks:

        - name: Add overlay and br_netfilter to modprobe
          blockinfile:
            create: yes
            path: /etc/modules-load.d/containerd.conf
            state: present
            block: |
                overlay
                br_netfilter
        
        - name: Load overlay and br_netfilter
          shell: |
            modprobe overlay
            modprobe br_netfilter
        
        - name: Setup required sysctl params, these persist across reboots.
          blockinfile:
            create: yes
            path: /etc/sysctl.d/99-kubernetes-cri.conf
            state: present
            block: |
                net.bridge.bridge-nf-call-iptables  = 1
                net.ipv4.ip_forward                 = 1
                net.bridge.bridge-nf-call-ip6tables = 1
        
        - name: Apply sysctl params without reboot
          shell: sysctl --system

    - name: Install containerd
      hosts: [masters, workers]
      become: yes
      vars_files:
        - ./vars/proxies.yaml
      tasks:
        
        - name: Create docker-archive-keyring.gpg file
          file:
            path: /usr/share/keyrings/docker-archive-keyring.gpg
            state: touch
            mode: u=rw,g=r,o=r
       
        - name: Add Docker apt key.
          environment: "{{proxy_env}}"
          apt_key:
            url: https://download.docker.com/linux/ubuntu/gpg
            state: present
            keyring: /usr/share/keyrings/docker-archive-keyring.gpg
 
        - name: Add Docker repository.
          apt_repository:
            repo: deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu   focal stable
            state: present
            update_cache: yes

        - name: Install containerd.io
          apt:
            name: containerd.io
            state: present
    
    - name: Configure containerd
      hosts: [masters, workers]
      become: yes
      tasks:
        - name: Check if containerd is correctly installed
          apt:
            name: containerd.io
            state: present
        
        - name: Create /etc/containerd directory
          file:
            path: /etc/containerd
            state: directory
        
        - name: Add containerd configuration in config.toml
          shell: containerd config default | sudo tee /etc/containerd/config.toml

        - name: Using the systemd cgroup driver 
          lineinfile:
            path: /etc/containerd/config.toml
            state: present
            insertafter: .containerd.runtimes.runc.options
            line: "            SystemdCgroup = true"

        - name: Restart service containerd, in all cases, also issue daemon-reload to pick up config changes
          systemd:
            state: restarted
            daemon_reload: yes
            name: containerd
    
    - name: Configure containerd proxies
      hosts: [masters, workers]
      become: yes
      vars_files:
        - ./vars/proxies.yaml
      tasks:

        - name: Check if containerd is correctly installed
          apt:
            name: containerd.io
            state: present
        
        - name: Create /etc/systemd/system/containerd.service.d directory
          file:
            path: /etc/systemd/system/containerd.service.d
            state: directory
        
        - name: Add proxy's rules in http_proxy.conf file
          blockinfile:
            create: yes
            path: /etc/systemd/system/containerd.service.d/http_proxy.conf
            state: present
            block: |
                [Service]
                Environment="HTTP_PROXY=http://proxy.enst-bretagne.fr:8080"
                Environment="HTTPS_PROXY=http://proxy.enst-bretagne.fr:8080"
        
        - name: Add no_proxy's rules in no_proxy.conf
          blockinfile:
            create: yes
            path: /etc/systemd/system/containerd.service.d/no_proxy.conf
            state: present
            block: |
                [Service]
                Environment="127.0.0.1,localhost,docker,.0,.1,.2,.3,.4,.5,.6,.7,.8,.9,.imt-atlantique.fr,.telecom-bretagne.eu,.enst-bretagne.fr"
        
        - name: Restart service containerd, in all cases, also issue daemon-reload to pick up config changes
          systemd:
            state: restarted
            daemon_reload: yes
            name: containerd
        

    - name: Cluster creation
      hosts: [masters]
      become: yes
      vars_files:
        - ./vars/proxies.yaml
      tasks:

        - name: Check if kubeadm, kukectl and kubelet are installed
          apt:
            pkg:
                - kubeadm
                - kubectl
                - kubelet
        
        - name: Kudeadm pull images
          shell: kubeadm config images pull
        
        - name: Kudeadm init cluster
          shell: kubeadm init
          ignore_errors: yes

        - name: Create /home/ubuntu/.kube/ directory
          file:
            path: /home/ubuntu/.kube/
            state: directory

        - name: Add current user to kubectl users group
          become: yes
          copy:
            src: /etc/kubernetes/admin.conf 
            remote_src: true
            dest: /home/ubuntu/.kube/config
            mode: u+rw,g-wx,o-rwx

        - name: Add user to file users
          become: yes
          shell: chown $(id -u ubuntu):$(id -g ubuntu) /home/ubuntu/.kube/config
        
        - name: Check if user is in kubctl users group
          become: false
          shell: kubectl get nodes
    
    - name: Install Weave-net (Network plugin)
      hosts: [masters]
      vars_files:
        - ./vars/proxies.yaml
      tasks:

        - name: Get kubectl version and hash it to base64
          shell: echo $(kubectl version | base64 | tr -d '\n')
          register: kubectl_version
        
        - set_fact:
            kuebctl_version_base64: "{{ kubectl_version.stdout }}"

        - name: Download the Weave-net networking manifest
          environment: "{{proxy_env}}"
          get_url:
            url: https://cloud.weave.works/k8s/net?k8s-version={{kuebctl_version_base64}}
            dest: $HOME/weave-net.yaml
          
        - name: Apply Weave-net
          environment: 
            CHECKPOINT_DISABLE: 1
          shell: kubectl apply -f $HOME/weave-net.yaml
    
    - name: Create join tokens
      hosts: [masters]
      tasks:

        - name: Retrieve join token
          shell: kubeadm token create --print-join-command > join_token.sh
          register: token
        
        - name: Save join command to a local file
          fetch:
            src: join_token.sh
            dest: ./tmp

        
    - name: Join Cluster
      hosts: [workers]
      tasks:

        - name: Download join tokens on node
          copy: 
            src: "./tmp/{{item}}/join_token.sh"
            dest: "/home/ubuntu/join_token_{{item}}.sh"
            mode: u=rxw,g=r,o=r
          with_items: "{{ groups['masters'] }}"
          ignore_errors: yes
        
        - name: Join cluster
          become: true
          command: sh "/home/ubuntu/join_token_{{item}}.sh"
          with_items: "{{ groups['masters'] }}"
          ignore_errors: yes

    - name: Install Helm
      hosts: [masters, workers]
      vars_files:
        - ./vars/proxies.yaml
      tasks:

        - name: Add Helm apt key.
          environment: "{{proxy_env}}"
          become: yes
          apt_key:
            url: https://baltocdn.com/helm/signing.asc
            state: present

        - name: Add Helm repository.
          environment: "{{proxy_env}}"
          become: yes
          apt_repository:
            repo: deb https://baltocdn.com/helm/stable/debian/ all main
            state: present
            update_cache: yes

        - name: Install Helm
          become: yes
          apt:
            name: helm
            state: present
            update_cache: yes

    - name: Install MetalLB
      hosts: [masters]
      vars_files:
        - ./vars/proxies.yaml
      tasks:

        - name: Create namespace
          environment: "{{proxy_env}}"
          shell: kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/namespace.yaml
          ignore_errors: yes
        
        - name: Install metalLB
          environment: "{{proxy_env}}"
          shell: kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.12.1/manifests/metallb.yaml

        - name: Copy configMap to host
          copy:
            src: ./config/metallb/metallb-config-map.yaml
            dest: .
            mode: 0644
        
        - name: Apply configMap
          shell: kubectl apply -f metallb-config-map.yaml
      
    - name: Install NGINX Ingress Controller
      hosts: [masters]
      vars_files:
        - ./vars/proxies.yaml
        - ./vars/nginx_ingress.yaml
      tasks:

        - name: Check if helm is installed
          become: yes
          apt:
            name: helm
            state: present
            update_cache: yes
        
        - name: Install NGINX Ingress Controller with helm
          environment: "{{proxy_env}}"
          shell: |
            helm upgrade --install ingress-nginx ingress-nginx \
            --repo https://kubernetes.github.io/ingress-nginx \
            --namespace ingress-nginx --create-namespace
        
        - name: Add custom DNS to /etc/hosts
          become: yes
          lineinfile:
            path: /etc/hosts
            state: present
            insertafter: "127.0.0.1 localhost"
            line: '{{nginx_ingress_ip}} glouglou.example.com'
        
        - name: Install Test service
          shell: >
            kubectl create deployment demo --image=httpd --port=80 &&
            kubectl expose deployment demo &&
            kubectl create ingress demo --class=nginx --rule="glouglou.example.com/*=demo:80"
          ignore_errors: yes
      
    - name: Install NGINX proxy
      hosts: [masters]
      become: yes
      vars_files:
        - ./vars/proxies.yaml
      tasks:

        - name: Install NGINX
          apt:
            name: nginx
            state: present
            update_cache: yes
        
        - name: Copy Nginx config file
          copy:
            src: ./config/nginx/custom_server.conf
            dest: /etc/nginx/sites-available/
            mode: 0644

        - name: Remove symlink
          file:
            path: "/etc/nginx/sites-enabled/default"
            state: absent
          
        - name: Create symbolic link 
          file:
            src: "/etc/nginx/sites-available/custom_server.conf"
            dest: "/etc/nginx/sites-enabled/custom_server.conf"
            state: link
            force: yes

        - name: Restart network service for interface eth0
          service:
            name: nginx
            state: restarted
