variable "external_network_name" {
  type    = string
  default = "external"
}

variable "external_router_name" {
  type    = string
}

variable "internal_subnet_name" {
  type = string
}

variable "internal_net_name" {
  type = string
}

variable "subnet_ip_range" {
  type    = string
  default = "192.168.0.0/24"
}

variable "enable_ext_net" {
  type    = bool
  default = false
}

variable "dns" {
  type = list(string)
}
