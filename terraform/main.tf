# ====
# Call module
# ====


# create network

module "deploy-newtork" {
  source               = "./os_network/"
  subnet_ip_range      = var.subnet_ip_range
  external_router_name = var.external_router_name
  internal_subnet_name = var.internal_subnet_name
  internal_net_name    = var.internal_net_name
  dns                  = var.dns
  enable_ext_net       = true
}

# create security group

module "worker-security-group" {
  source              = "./os_sec_group"
  sec_group_name      = "worker-security-group"
  ingress_allow_ports = var.worker_allow_ports
}

module "master-security-group" {
  source              = "./os_sec_group"
  sec_group_name      = "master-security-group"
  ingress_allow_ports = var.master_allow_ports
}

module "ssh-icmp-security-group" {
  source              = "./os_sec_group"
  sec_group_name      = "ssh-icmp-security-group"
  ingress_allow_ports = var.icmp_ssh_allow_ports
}

module "http-https-security-group" {
  source              = "./os_sec_group"
  sec_group_name      = "http-https-security-group"
  ingress_allow_ports = var.http_https_allow_ports
}

module "weave-net-security-group" {
  source              = "./os_sec_group"
  sec_group_name      = "weave-net-security-group"
  ingress_allow_ports = var.weave_net_allow_ports
}

module "metallb-security-group" {
  source ="./os_sec_group"
  sec_group_name= "metallb-security-group"
  ingress_allow_ports = var.metallb-allow-ports
}

# create workers

module "deploy-worker-instances" {
  source               = "./os_instance/"
  nb_instance          = var.nb_workers
  instance_name        = "worker"
  enable_fip           = false
  subnet_ip_range      = var.subnet_ip_range
  external_router_name = var.external_router_name
  internal_subnet_name = var.internal_subnet_name
  internal_net_name    = var.internal_net_name
  dns                  = var.dns
  image_name           = "imta-ubuntu"
  flavor_name          = "s10.large"
  ssh_key_name         = var.ssh_key_name
  security_groups = [
    "worker-security-group",
    "ssh-icmp-security-group",
    "http-https-security-group",
    "weave-net-security-group",
    "metallb-security-group",
    "default"
  ]
  depends_on = [
    module.deploy-newtork,
    module.worker-security-group,
    module.ssh-icmp-security-group,
    module.http-https-security-group,
    module.metallb-security-group
  ]
}

# create masters

module "deploy-master-instances" {
  source               = "./os_instance/"
  instance_name        = "master"
  nb_instance          = var.nb_masters
  enable_fip           = true
  subnet_ip_range      = var.subnet_ip_range
  external_router_name = var.external_router_name
  internal_subnet_name = var.internal_subnet_name
  internal_net_name    = var.internal_net_name
  dns                  = var.dns
  image_name           = "imta-ubuntu"
  flavor_name          = "s10.medium"
  ssh_key_name         = var.ssh_key_name
  security_groups = [
    "master-security-group",
    "ssh-icmp-security-group",
    "http-https-security-group",
    "weave-net-security-group",
    "metallb-security-group",
    "default"
  ]
  depends_on = [
    module.deploy-newtork,
    module.master-security-group,
    module.ssh-icmp-security-group,
    module.http-https-security-group,
      module.metallb-security-group
  ]
}

# create bastions
module "deploy-bastion-instances" {
  source               = "./os_instance/"
  instance_name        = "bastion"
  nb_instance          = var.nb_bastion
  enable_fip           = true
  subnet_ip_range      = var.subnet_ip_range
  external_router_name = var.external_router_name
  internal_subnet_name = var.internal_subnet_name
  internal_net_name    = var.internal_net_name
  dns                  = var.dns
  image_name           = "imta-ubuntu"
  flavor_name          = "s10.small"
  ssh_key_name         = var.ssh_key_name
  security_groups = [
    "ssh-icmp-security-group",
    "default"
  ]
  depends_on = [
    module.deploy-newtork,
    module.ssh-icmp-security-group,
  ]
}


# create metalLB IP
module "create-metallb-ip" {
  source = "./os_floating_ip"
}