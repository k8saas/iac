# ===
# Network
# ===

data "openstack_networking_network_v2" "ext_network" {
  name = var.external_network_name
}

data "openstack_networking_router_v2" "ext_router" {
  name = var.external_router_name
}

data "openstack_networking_network_v2" "internal_net" {
  name           = var.internal_net_name
}

data "openstack_networking_subnet_v2" "internal_sub" {
  name            = var.internal_subnet_name
}

resource "openstack_networking_floatingip_v2" "fip_http" {
  count = var.enable_fip ? 1 : 0
  pool  = data.openstack_networking_network_v2.ext_network.name
}
