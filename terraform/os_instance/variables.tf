variable "external_network_name" {
  type    = string
  default = "external"
}

variable "external_router_name" {
  type    = string
}

variable "internal_subnet_name" {
  type = string
}

variable "internal_net_name" {
  type = string
}

variable "dns" {
  type = list(string)
}

variable "subnet_ip_range" {
  type    = string
}

variable "instance_name" {
  type    = string
}

variable "image_name" {
  type    = string
  default = "imta-ubuntu"
}

variable "flavor_name" {
  type    = string
  default = "m1.small"
}

variable "enable_fip" {
  type    = bool
  default = false
}

variable "nb_instance" {
  type    = number
  default = 1
}

variable "ssh_key_name" {
  type = string
}
variable "security_groups" {
  type = list
  default = ["default"]
}