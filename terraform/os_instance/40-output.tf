resource "local_file" "ip" {
  count           = var.nb_instance
  content = "${openstack_compute_instance_v2.std_srv[count.index].network[0].fixed_ip_v4}"
  filename = "../resources/${var.instance_name}s/${var.instance_name}-${count.index}-local-ip"
}

resource "local_file" "fip" {
  count           = var.enable_fip ? 1 : 0
  content = "${openstack_networking_floatingip_v2.fip_http[0].address}"
  filename = "../resources/${var.instance_name}s/${var.instance_name}-${count.index}-public-ip"
}