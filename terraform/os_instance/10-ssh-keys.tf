# ===
# SSH key pair
# ===

#Selected SSH key pair
data "openstack_compute_keypair_v2" "user_key" {
  name       = var.ssh_key_name
}