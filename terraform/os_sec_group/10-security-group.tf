# ===
# Security Groups
# ===


resource "openstack_compute_secgroup_v2" "security_group" {
  name        = "${var.sec_group_name}"
  description = "${var.sec_group_name} security group"

  dynamic "rule" {
    for_each = var.ingress_allow_ports
    content {
      from_port   = tonumber(rule.value["from_port"])
      to_port     = tonumber(rule.value["to_port"])
      ip_protocol = rule.value["ip_protocol"]
      cidr        = "0.0.0.0/0"
    }
  }
}

