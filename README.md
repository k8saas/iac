# IaC

***Attention, les commandes ci-dessous doivent être exécutées depuis une machine disposant d'un accès à Openstack ainsi que des droits sudo pour installer les différents logiciels (donc soit la VDI soit par le VPN)***

## Quickstart

Dans cette partie nous allons deployer un cluster K8S sur Openstack en mode bare metal (c'est à dire sans utiliser le Load Balancer). Le instances deployées seront les suivantes:

* Un Master Node (qui jouera également le rôle d'Edge node, rendant le cluster accessible depuis l'exterieur),
* Trois Worker Nodes sur lesquelles seront repartie la charge de travaille,
* Un bastion SSH afin de permettre a asnible d'éxectuer des tâches sur les différents nodes tout en limitant l'utilisation d'adresses IP publiques.

### Prérequis

Avant de deployer notre cluster, il faut s'assurer que notre poste de travail est corectement configuré. Assurez-vous que les programmes suivants sont installés sur votre machine:
* [Terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli) >= v1.1.3
* [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) >= 2.9.6
* [pipenv](https://pypi.org/project/pipenv/) >= 11.9.0

### Deployement de l'insfrastructure avec Terraform

Pour deployer votre infrastructure sans ambuche, assurez-ous de disposez des resources suivantes dans votre projet Openstack:
* 2 IP flottantes
* 5 instances
* 28 règles de securité.
* 6 groupes de sécurité
* 1 clef SSH préalablement créée et présente sur votre machine (le .pem)

#### Chargement de l'environnement Openstack

SUr votre compte Openstack, télécharger le fichier *-openrc.sh de votre projet en cliquant sur votre pseudo (en haut à droite, puis `Fichier OpenStack RC`). Une fois ce fichier téléchargé, placer le dans votre repertoire courrant puis chargé le avec la commande:

```
source monfichier-openrc.sh
```

Entrez votre mot de passe Openstack lorsque celui-ci vous est demandé. Attention, il faudra charger ce fichier à chaque ouverture d'un nouveau terminal.

Déplacer vous dans le dossier terraform: 

```
cd terraform
```

Rendez-vous dans le fichier `env.tf` puis, au niveau de la variable `ssh_key_name`, renseignez le nom de votre clef SSH enregistrée sur Openstack dans la valeur `default` (**donc sans .pem à la fin et comme afficher dans l'onglet *Compute > Paires de clefs* sur Openstack**).

#### Execution des recettes terraforme

Une fois l'environnement Openstack de votre projet chargé dans votre terminal, executez les commandes suivantes pour créer votre infrastructure:

```
terraform init 
```
puis
```
terraform apply
```
 Entrez `yes` pour valider le déployement.

 Une fois que le deployement est terminé, rendez-vous sur openstack pour verifier que tout c'est bien installer.

 Si vous souhaitez detruire votre installation, il vous suffit d'entrez la commande suivante:

 ```
 terraform destroy 
 ```

 PS: Il se peut qu'une erreur intervienne lors de la supression de l'infrastructure (problème de suppression de la règle de sécurité de Weave-net qui échoue car Terraform essaye de l supprimer trop tôt). Dans ce cas, relancer la commande une seconde fois et tout devrait rentrer dans l'ordre.

 ### Installation du cluster avec Ansible 

 Une fois que votre infrastructure est en place, rendez-vous dans le repertoire `ansible`:
 
 ```
 cd ../ansible
 ```
 Là, créez un fichier `.env` inspiré du fichier `.env.template`:
 ```
 cp .env.template .env
 ```
 Modifiez la valeur de `OS_KEY_PATH` en indiquant le chemin vers votre clef privée openstack.

 Une fois cela fait, éxecutezz la commande suivante afin de génrer le fichier `hosts` d'ansible:

 ```
 pipenv run python load_hosts.py
 ```

 Une fois cela fait, un fichier `hosts` devrait apparaitre dans votre répertoire. Ouvrez le et verifiez que les adresses IP rentrées correspondent bien à celles de votre infrastructure Openstack.
 
 Une fois cela fait, recuperez l'adresse IP privée du master et ajouter la à la liste no_proxy présent dans le fichier `./vars/proxies.yaml`.

 Ensuite, choisissez une adresse IP non utilisée deu réseau K8s_network (adresse en 172.16.0.*) et reseigner la dans le fichier `vars/nginx_ingress.yaml` ainsi que dans le fichier `config/metallb-config-map.yaml` (mettre 172.16.0.X-172.16.0.X avec * la valeur que vous avez choisie).

 Une fois la verification faite, lancez le playbook ansible avec la commande:

 ```
 ansible-playbook -i hosts playbook.yaml
 ```

 Ansible va alors vous demandez de valider la connection avec les hôtes: entrez yes autant de fois que demander. Si besoin, faites un `Ctrl-C` puis relancez le playbook jusqu'a ce que toutes les connections soitent validées.

 Une fois le playbook exécuté, rendez-vous depuis un navigateur à l'adresse IP publique de votre Master Node (qui est également le edge node). It works!